package ma.octo.assignement.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.*;
import ma.octo.assignement.service.AuditService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(TransferController.class)
class TransferControllerTest {
    @MockBean
    private AuditService As;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CompteRepository rep1;

    @MockBean
    private TransferRepository re2;
    //on ajoute le repository de deposit
    @MockBean
    private MoneyDepositRepository re4;

    @MockBean
    private AuditDepositRepository ADRep;

    @MockBean
    private AuditTransferRepository ATRep;
    @MockBean
    private  UtilisateurRepository re3;

    @Autowired
    private MockMvc mockMvc;

    /*@Test
    void loadAllTransfersTest() throws Exception {
        Compte c1 = new Compte();
        Compte c2 = new Compte();

        List<Transfer> tutorials = new ArrayList<>(
                Arrays.asList(new Transfer(3L, BigDecimal.valueOf(200L),new Date(),c1,c2,"transfer 1 "),
                        new Transfer(9L, BigDecimal.valueOf(200L),new Date(),c1,c2,"transfer 1 "),
                        new Transfer(5L, BigDecimal.valueOf(200L),new Date(),c1,c2,"transfer 1 ")));

        when(re2.findAll()).thenReturn(tutorials);
        mockMvc.perform(get("/listDesTransferts"))
                .andExpect(status().isOk())

                .andDo(print());

    }*/

 /*   @Test
    void loadAllCompteTest() throws Exception {

        Utilisateur u1 = new Utilisateur();
        Utilisateur u2 = new Utilisateur();

        List<Compte> tutorials = new ArrayList<>(
                Arrays.asList(new Compte(3L,"1020304050607080","1020304050607820",BigDecimal.valueOf(2030000L),u1),
                        new Compte(4L,"1020304050600080","1020304050600020",BigDecimal.valueOf(2030400L),u2 )));

        when(rep1.findAll()).thenReturn(tutorials);
        mockMvc.perform(get("listOfAccounts"))
                .andExpect(status().isOk())

                .andDo(print());

    }
*/
    //@Test
   /* void loadAllUtilisateurTest() throws Exception {


        List<Utilisateur> tutorials = new ArrayList<>(
                Arrays.asList(new Utilisateur(4L,"Wissal","Female","Wissal","FELIOUNE",null),
                        new Utilisateur(5L,"Wissal","Female","Wissal","FELIOUNE",null)));

        when(re3.findAll()).thenReturn(tutorials);
        mockMvc.perform(get("lister_utilisateurs"))
                .andExpect(status().isOk())
                .andDo(print());

    }*/

    //@Test
    /*void createTransactionTest() throws Exception {
        Compte c1 = new Compte();
        Compte c2 = new Compte();
        Transfer tutorial = new Transfer(1L,BigDecimal.valueOf(100L),new Date(),c1,c2,"transfert 1 ");

        mockMvc.perform(post("/executerTransfers").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(tutorial)))
                .andExpect(status().isCreated())
                .andDo(print());







    }*/

    @Test
    void createTransaction2Test() throws Exception{
        Utilisateur u = new Utilisateur(6L,"user1","Female","last","first",null);
       re3.saveAndFlush(u);
         Compte c = new Compte(2L,"010000A000001000","RIB64",BigDecimal.valueOf(150000L),u);
         rep1.saveAndFlush(c);

        DepositDto tutorial = new DepositDto("RIB4","Wissalelioune","depot",BigDecimal.valueOf(200L),new Date());

        mockMvc.perform(post("/executerDeposit").contentType(MediaType.APPLICATION_JSON)
                 .content(objectMapper.writeValueAsString(tutorial)))
                .andDo(print());

    }
}