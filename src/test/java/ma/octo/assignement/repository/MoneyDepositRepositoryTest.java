package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class MoneyDepositRepositoryTest {



    @Autowired
    private MoneyDepositRepository MoneyDepositRepository;

    @Autowired
    private CompteRepository C1Rep;



   /* @Test
    public void should_find_all_Deposit_by_Nom_Emmetteur() {
        Compte c1 = new Compte();
        MoneyDeposit Md1 = new MoneyDeposit(8L, BigDecimal.TEN, new Date(),
                "User full name test 1 ", c1, "Depot test 1 ");

        C1Rep.save(c1);
        MoneyDepositRepository.save(Md1);
        List<MoneyDeposit> foundDeposit = MoneyDepositRepository.findByNomPrenomEmetteur(Md1.getNomPrenomEmetteur());
        assertThat(foundDeposit).contains(Md1);


    }*/
   /* @Test
    public void should_find_all_Deposit_by_Compte_Benif() {
        Compte c1 = new Compte();
        MoneyDeposit Md1 = new MoneyDeposit(8L, BigDecimal.TEN, new Date(),
                "User full name test 1 ", c1, "Depot test 1 ");

        C1Rep.save(c1);

        MoneyDepositRepository.save(Md1);

        List<MoneyDeposit> foundDeposit = MoneyDepositRepository.findByCompteBeneficiaire(Md1.getCompteBeneficiaire());

        assertThat(foundDeposit).contains(Md1);



    }
*/
    @Test
    public void should_Find_All_deposit() {
        Compte c1 = new Compte();
        Compte c2 = new Compte();
        Compte c3 = new Compte();
        C1Rep.save(c1);
        C1Rep.save(c2);
        C1Rep.save(c3);
        MoneyDeposit Md1 = new MoneyDeposit(8L, BigDecimal.TEN, new Date(),
                "User full name test 1 ", c1, "Depot test 1 ");
        MoneyDepositRepository.save(Md1);

        MoneyDeposit Md2 = new MoneyDeposit(9L, BigDecimal.TEN, new Date(),
                "User full name test 2 ", c2, "Depot test 2 ");
        MoneyDepositRepository.save(Md2);

        MoneyDeposit Md3 = new MoneyDeposit(9L, BigDecimal.TEN, new Date(),
                "User full name test 3 ", c3, "Depot test 3 ");

        MoneyDepositRepository.save(Md3);

        List<MoneyDeposit> tutorials = MoneyDepositRepository.findAll();
        assertThat(tutorials).hasSizeGreaterThanOrEqualTo(3);


    }

    @Test
    public void Should_store_a_deposit_Test() throws CompteNonExistantException {
        Compte c1 = new Compte();
        C1Rep.save((c1));
        MoneyDeposit expectedmd = new MoneyDeposit(1L, BigDecimal.TEN, new Date(),
                "User full name test 1 ", c1, "Depot test 1 ");

            //On sticke repository
            MoneyDeposit savedMd = MoneyDepositRepository.save(expectedmd);
            //on s'assure que tout est normale
            assertNotNull(savedMd);


    }

    @Test
    public void should_delete_a_deposit_by_id() {
        Compte c1 = new Compte();
        C1Rep.save(c1);
        MoneyDeposit Md1 = new MoneyDeposit(1L, BigDecimal.TEN, new Date(),
                "User full name test 1 ", c1, "Depot test 1 ");
        MoneyDeposit m2= MoneyDepositRepository.save(Md1);

        MoneyDepositRepository.deleteById(m2.getId());
        Optional<MoneyDeposit> m = MoneyDepositRepository.findById(Md1.getId());

       assertTrue(m.isEmpty());


    }

}