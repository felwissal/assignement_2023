package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	@Autowired
	private TransferRepository transferRepository;
	//ajouté
	@Autowired
	private MoneyDepositRepository depositRepository;

	public static void main(String[] args) {
		SpringApplication.run(NiceBankApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("username1");
		utilisateur1.setPassword("$2a$12$UP75qDw5KNP7g9X3Ejh2NOWkUnCKarWLk4ecTmX/niCrZmq71wf8y");

		utilisateur1.setLastname("last1");
		utilisateur1.setFirstname("first1");
		utilisateur1.setGender("Male");
		utilisateur1.setRole("ADMIN");
		utilisateurRepository.save(utilisateur1);


		Utilisateur utilisateur2 = new Utilisateur();
		utilisateur2.setUsername("username2");
		utilisateur2.setLastname("last2");
		utilisateur2.setPassword("$2a$12$UP75qDw5KNP7g9X3Ejh2NOWkUnCKarWLk4ecTmX/niCrZmq71wf8y");
		utilisateur2.setFirstname("first2");
		utilisateur2.setGender("Female");
		utilisateur2.setRole("USER");

		utilisateurRepository.save(utilisateur2);


		Compte compte1 = new Compte();
		compte1.setNrCompte("010000A000001000");
		compte1.setRib("RIB1");
		compte1.setSolde(BigDecimal.valueOf(800000L));
		compte1.setUtilisateur(utilisateur1);

		compteRepository.save(compte1);

		Compte compte2 = new Compte();
		compte2.setNrCompte("010000B025001000");
		compte2.setRib("RIB4");
		compte2.setSolde(BigDecimal.valueOf(150000L));
		compte2.setUtilisateur(utilisateur2);

		compteRepository.save(compte2);

		Transfer v = new Transfer();
		v.setMontantTransfer(BigDecimal.TEN);
		v.setCompteBeneficiaire(compte2);
		v.setCompteEmetteur(compte1);
		v.setDateExecution(new Date());
		v.setMotifTransfer("Assignment 2021");

		transferRepository.save(v);
		MoneyDeposit ver = new MoneyDeposit();
		ver.setMontant(BigDecimal.valueOf(20L));
		ver.setCompteBeneficiaire(compte2);
		ver.setNomPrenomEmetteur("Wissal FELIOUNE2");
		ver.setDateExecution(new Date());
		ver.setMotifDeposit("Versement2");

		depositRepository.save(ver);
	}
}
