package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter@Setter
public class DepositDto {

	private String ribCompteBeneficiaire;
	private String nomPrenomEmmeteur;
	private String motif;
	private BigDecimal montant;
	private Date date;

}


