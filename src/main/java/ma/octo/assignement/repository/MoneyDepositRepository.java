package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;

import java.util.List;
import java.util.Optional;

public interface MoneyDepositRepository extends JpaRepository<MoneyDeposit, Long>{

    List<MoneyDeposit> findByNomPrenomEmetteur(String nomPrenomEmetteur);

    @Override
    void deleteAllById(Iterable<? extends Long> longs);


    List<MoneyDeposit> findByCompteBeneficiaire(Compte c1);


}
