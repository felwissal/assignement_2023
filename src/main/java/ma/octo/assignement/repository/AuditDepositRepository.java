package ma.octo.assignement.repository;

import ma.octo.assignement.domain.util.EventType;
import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.AuditDeposit;

import java.util.Optional;

public interface AuditDepositRepository extends JpaRepository<AuditDeposit, Long> {

    Optional<AuditDeposit> findByEventType(EventType e);

}
