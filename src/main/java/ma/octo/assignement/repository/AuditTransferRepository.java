package ma.octo.assignement.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuditTransferRepository extends JpaRepository<AuditTransfer, Long> {
	
	//le nom de la classe ne convient pas avec  le nom citué ci dessus 
	// le nom de la class est AuditVirementRepository et la class au dessus est auditTransferRepository
	//après avoir fixé cette erreu un autre s'affiche au niveau de service Audit service

    Optional<AuditTransfer> findByEventType(EventType e);
	
}
